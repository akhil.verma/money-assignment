import { Budgets } from "src/app/model/BudgetResponse";

export function getTestBudgets(): Budgets[] {
    return [
      {id: "41", name: 'Test1', last_modified_on:"2021-01-01",first_month:"",last_month:"",accounts:[]},
      {id: "42", name: "Test2", last_modified_on:"2021-01-01",first_month:"",last_month:"",accounts:[]},
      {id: "43", name: 'Test3', last_modified_on:"2021-01-01",first_month:"",last_month:"",accounts:[]}

    ];
  }