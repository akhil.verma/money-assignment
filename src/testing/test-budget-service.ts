import { Injectable } from "@angular/core";
import { CommonService } from "src/app/shared/services/commonservices.service";
import { getTestBudgets } from "./budget-test";

@Injectable()
export class TestBudgetService extends CommonService{
    constructor(){
        super(null);
    }

    budgets = getTestBudgets();
}