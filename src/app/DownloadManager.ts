import { Injectable } from '@angular/core';
import { Router } from '@angular/router'
import { CommonService } from './shared/services/commonservices.service';
import { BaseComponent } from './BaseComponent';
import { TaskCode } from './Utils/GlobalConstants';
import { HttpRequest } from './model/HttpRequest';

export class DownloadManager {
    router: Router;
    protected commonService: CommonService;
    protected c: BaseComponent;
    constructor(c: BaseComponent, service: CommonService) {
        this.c = c;
        this.commonService = service;
    }

    downloadData(req: HttpRequest) {
        this.onPreExecute(req.taskcode);
        this.commonService.callHttpReq(req)
            .subscribe(
            res => {
                this.onResponseReceived(req.taskcode, res, req);
            },
            error => {
                console.log(error.status);
                console.log(error);
                if (error.status === 401) {
                    this.logOutUser();
                }else if(error.status === 0){
                    this.onInternetOrTimeOutErrorReceived(req.taskcode);
                } else {
                    let errorRes = JSON.parse(error._body);
                    // this.onErrorReceived(req.taskcode, JsonParser.parseJsonString(errorRes, req.classTypeValue));
                }
            },
            () => {
            }
            );
    }
    onPreExecute(taskCode: TaskCode) {
        this.c.onPreExecute(taskCode);
    }
    onErrorReceived(taskCode: TaskCode, res:any) {
            
    //    lert('error in api calling with taskcode =' + taskCode); a
        this.c.onErrorReceived(taskCode, res);


    }
    onInternetOrTimeOutErrorReceived(taskCode:TaskCode){
        this.c.onInternetOrTimeoutErrorReceived(taskCode);
    }

    logOutUser() {
        
    }

    onResponseReceived(taskCode: TaskCode, response: any, req: HttpRequest) {
        this.c.onResponseReceived(taskCode, response);
    }

    // parseJson(response: any, req: HttpRequest) {
    //     if (req.isArrayResponse) {
    //         return JsonParser.parseJsonArray(response, req.classTypeValue);
    //     } else {
    //         return JsonParser.parseJsonString(response, req.classTypeValue);
    //     }

    // }
}
