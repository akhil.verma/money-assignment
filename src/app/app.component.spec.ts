import { Component, DebugElement } from '@angular/core';
import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { RouterLinkDirectiveStub } from 'src/testing/router-link-directive-stub';
import { AppComponent } from './app.component';
import { By } from '@angular/platform-browser';


@Component({selector: 'app-header', template: ''})
class HeaderStubComponent {
}

@Component({selector: 'router-outlet', template: ''})
class RouterOutletStubComponent {
}

@Component({selector: 'app-sidenav', template: ''})
class SideNavStubComponent {
}
let comp: AppComponent;
let fixture: ComponentFixture<AppComponent>;

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent,HeaderStubComponent,RouterLinkDirectiveStub,
        SideNavStubComponent,RouterOutletStubComponent
      ],
    })
    .compileComponents()
    .then(()=>{
      fixture = TestBed.createComponent(AppComponent);
      comp = fixture.componentInstance;
    });

  }));
  tests();
});

import { AppModule } from './app.module';
import { AppRoutingModule } from './app-routing.module';

describe('AppComponent & AppModule', () => {
  beforeEach(async(() => {
    TestBed
        .configureTestingModule({imports: [AppModule]})

        // Get rid of app's Router configuration otherwise many failures.
        // Doing so removes Router declarations; add the Router stubs
        .overrideModule(AppModule, {
          remove: {imports: [AppRoutingModule]},
          add: {declarations: [RouterLinkDirectiveStub, RouterOutletStubComponent]}
        })

        .compileComponents()

        .then(() => {
          fixture = TestBed.createComponent(AppComponent);
          comp = fixture.componentInstance;
        });
  }));

  tests();
});

function tests(){
  let routerLinks: RouterLinkDirectiveStub[];
  let linkDes: DebugElement[];

  beforeEach(() =>{
    fixture.detectChanges();
    linkDes = fixture.debugElement.queryAll(By.directive(RouterLinkDirectiveStub));

    routerLinks = linkDes.map(de => de.injector.get(RouterLinkDirectiveStub));

    it('can instantiate the component', () => {
      expect(comp).not.toBeNull();
    });

    it(`should have as title 'ynab'`, () => {
    
      expect(comp.title).toEqual('ynab');
    });

    it('can get RouterLinks from template', () => {
      expect(routerLinks.length).toBe(3, 'should have 3 routerLinks');
      expect(routerLinks[0].linkParams).toBe('/all-budget');
      expect(routerLinks[1].linkParams).toBe('/budget-accounts');
      expect(routerLinks[2].linkParams).toBe('/payees-data');
    });

    it('can click Budgets link in template', () => {
      const budgetsLinkDe = linkDes[0];    // budget link DebugElement
      const budgetsLink = routerLinks[0];  // budget link directive
  
      expect(budgetsLink.navigatedTo).toBeNull('should not have navigated yet');
  
      budgetsLinkDe.triggerEventHandler('click', null);
      fixture.detectChanges();
  
      expect(budgetsLink.navigatedTo).toBe('/all-budget');
    });
  })
}
