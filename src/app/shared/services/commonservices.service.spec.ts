import { HttpClient, HttpClientModule } from '@angular/common/http';
import { async, inject, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { CommonService } from './commonservices.service';
let service: CommonService;
  let httpMock: HttpTestingController;
  let httpClient: HttpClient;
describe('CommonService', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[
        HttpClientModule,
        HttpClientTestingModule
      ]
    }).compileComponents();
    service = TestBed.inject(CommonService);
    httpMock = TestBed.get(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);
  });

  it('should be created', async(inject([HttpTestingController, CommonService],
    (httpClient: HttpTestingController, commonService: CommonService) => {
      expect(commonService).toBeTruthy();
    })));
});
