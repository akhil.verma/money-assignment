import { Injectable } from '@angular/core';
import { HttpRequest } from 'src/app/model/HttpRequest';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  constructor(private httpClient: HttpClient) { }

  callHttpReq(req: HttpRequest):Observable<any> {
    const options = { headers: req.headers };
    
    switch(req.method){
      case 'GET':
        return this.httpClient.get(req.url, options);
      case 'POST':
        return this.httpClient.post(req.url,req.params,options);
      case 'PATCH':
        return this.httpClient.patch(req.url, req.params, options);
      case 'DELETE':
        return this.httpClient.delete(req.url, options);
    }
}

}

