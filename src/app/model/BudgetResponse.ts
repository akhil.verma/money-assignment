import { BaseResponse } from './BaseResponse';

export class BudgetResponse extends BaseResponse{
    data:ListBudget;
}
export class AccountResponse extends BaseResponse{
    data: BudgetAccount;
}
export class BudgetAccount{
    accounts: AccountData[];
    server_knowledge:number;
}
export class ListBudget{
    budgets: Budgets[];
    default_budget: string;
}

export class Budgets{
    id:string;
    name: string;
    last_modified_on:string;
    first_month: string;
    last_month:string;
    accounts:AccountData[];

}



export class AccountData{
    id:string;
    name:string;
    type:string;
    on_budget:boolean;
    closed:boolean;
    note:string;
    balance:number;
    cleared_balance:number;
    deleted:boolean;
    transfer_payee_id:string;
}

export class CreateAccountDao{
    name:string;
    type:string;
    balance:number;
}

export class PayeeResponse{
    data:PayeeData;
}
export class Payee {
    id: string;
    name: string;
    transfer_account_id: string;
    deleted: boolean;
}

export class PayeeData {
    payees: Payee[];
    server_knowledge: number
}

export class TransactionResponse{
    data:TransactionData;
}
export class TransactionData{
    transactions:Transaction[];
}
export class Transaction{
    date:string;
    amount:number;
    account_name:string;
    category_name:string;
    payee_name:string;
    deleted:boolean;
    memo:string;
    account_id:string;
    payee_id:string;
    cleared:string;
    approved:boolean;
    flag_color:string;
}
