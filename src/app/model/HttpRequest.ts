import { TaskCode } from './../Utils/GlobalConstants';
import { HttpHeaders } from '@angular/common/http';

export class HttpRequest{
    url: string;
    method: string;
    params: any;
    taskcode: TaskCode;
    headers: HttpHeaders;

    constructor(url: string) {
        this.url = url;
        this.method = "GET";
        this.headers = new HttpHeaders().append('Authorization',
        'Bearer 39ab4a29d6a7f2a803f50e1cbf5618263e63b01d2c42cb3c8ce5b0e00702f5a4');    
        this.addHeader("Content-Type","application/json");
        this.addHeader('Cache-Control','no-cache');
    }
    setPostMethod() {
        this.method = "POST";
    }
    setPutMethod() {
        this.method = "PUT";
    }
    setDeleteMethod() {
        this.method = "DELETE";
    }

    setPatchMethod() {
        this.method = "PATCH";
    }

    addHeader(key: string, value: string) {
        this.headers.append(key, value);
    }
    removeHeader(key: string) {
        this.headers.delete(key);
    }
}