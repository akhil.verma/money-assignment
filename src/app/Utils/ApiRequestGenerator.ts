import { CreateAccountDao } from '../model/BudgetResponse';
import { HttpRequest } from './../model/HttpRequest';
import * as GlobalConstants from './../Utils/GlobalConstants';
import { TaskCode } from './../Utils/GlobalConstants';
import { Transaction } from './../model/BudgetResponse';


export class ApiRequestGenerator{
    static getAllBudgets(){
        let httpReq = new HttpRequest(GlobalConstants.GET_ALL_BUDGET);
        httpReq.taskcode = TaskCode.GET_ALL_BUDGET;
        return httpReq;
    }

    static getAccountByBudgetId(id:string){
        let httpReq = new HttpRequest(GlobalConstants.GET_ALL_BUDGET + '/'+id+'/accounts');
        httpReq.taskcode = TaskCode.GET_ACCOUNTS_BY_BUDGET;
        return httpReq;
    }
    static createAccount(req:CreateAccountDao,id:string){
        let httpreq = new HttpRequest(GlobalConstants.GET_ALL_BUDGET+'/'+id+'/accounts');
        httpreq.setPostMethod();
        httpreq.params = {account: req};
        httpreq.taskcode = TaskCode.CREATE_ACCOUNT;
        return httpreq;
    }
    static getPayeesByBudget(id){
        let httpreq = new HttpRequest(GlobalConstants.GET_ALL_BUDGET + '/'+ id + '/payees');
        httpreq.taskcode = TaskCode.GET_PAYEES_BY_BUDGET;
        return httpreq;        
    }
    static getAllTrasactionByPayee(budgetId:string, payeeId:string){
        let httpreq = new HttpRequest(GlobalConstants.GET_ALL_BUDGET+'/'+ budgetId+'/payees/'+payeeId+'/transactions');
        httpreq.taskcode = TaskCode.GET_ALL_TRANSACTION;
        return httpreq;
    }
    static createTransaction(request: Transaction,budgetId:string){
        let httpreq = new HttpRequest(GlobalConstants.GET_ALL_BUDGET+'/'+ budgetId+'/transactions');
        httpreq.setPostMethod();
        httpreq.params = { transaction : request};
        httpreq.taskcode = TaskCode.CREATE_TRANSACTION;
        return httpreq;
    }
}