'use strict'
export const BASE_URL:string = 'https://api.youneedabudget.com/v1';

export const GET_ALL_BUDGET:string = BASE_URL + '/budgets';

export enum TaskCode{
    GET_ALL_BUDGET,
    GET_ACCOUNTS_BY_BUDGET,
    CREATE_ACCOUNT,
    GET_PAYEES_BY_BUDGET,
    GET_ALL_TRANSACTION,
    CREATE_TRANSACTION
}
export const ACCOUNT_TYPE_ARRAY = [
    'Checking',
    'Savings',
    'Cash',
    'Credit Card',
]