import { OnInit } from '@angular/core';
import { TaskCode } from './Utils/GlobalConstants';
import { HttpRequest } from './model/HttpRequest';
import { CommonService } from './shared/services/commonservices.service';
import { DownloadManager } from './DownloadManager';
import { BaseResponse } from './model/BaseResponse';


export class BaseComponent implements OnInit {

  constructor(protected commonservice: CommonService){

  }

  ngOnInit() {
  }

  showLoader() {
  }

  stopLoader() {
  }

  downloadData(req: HttpRequest) {
    const manager = new DownloadManager(this, this.commonservice);
    manager.downloadData(req);
  }

  onPreExecute(taskCode: TaskCode) {
    this.showLoader();
  }

  onErrorReceived(taskCode: TaskCode, res: any) {
    this.showErrorMessage(taskCode, res.message);
    this.stopLoader();
  }
  onInternetOrTimeoutErrorReceived(taskCode:TaskCode){
    alert("Something went wrong. Please try again...");
    this.stopLoader();
  }

  logOut() {
  }

  showErrorMessage(taskCode: TaskCode, message: string) {
    console.log(message);
    // if (!CheckUtil.isNullEmptyString(message)) {
    //   alert(message);
    // }
  }

  onResponseReceived(taskCode: TaskCode, response: any) {
    // Logger.log(response);
    this.stopLoader();
    if (response instanceof BaseResponse) {
      if (response.error && response.message) {
        this.showErrorMessage(taskCode, response.message);
      }
      return !response.error;
    }
    return true;
  }

}
