import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllBudgetComponent } from './components/all-budget/all-budget.component';
import { AccountListComponent } from './components/account-list/account-list.component';
import { PayeesListComponent } from './components/payees-list/payees-list.component';

const routes: Routes = [
  {
    path: 'all-budget',
    component: AllBudgetComponent
  },
  {
    path: 'budget-accounts',
    component: AccountListComponent
  },
  {
    path: 'payees-data',
    component:PayeesListComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],

exports: [RouterModule]
})
export class AppRoutingModule { }
