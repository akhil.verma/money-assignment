import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { HeaderComponent } from './shared/components/header/header.component';
import { SidenavComponent } from './shared/components/sidenav/sidenav.component';
import { AllBudgetComponent } from './components/all-budget/all-budget.component';
import { AccountListComponent } from './components/account-list/account-list.component';
import { CreateAccountComponent } from './components/account-list/create-account/create-account.component';
import { FormsModule } from '@angular/forms';
import { PayeesListComponent } from './components/payees-list/payees-list.component';
import { TransactionListComponent } from './components/payees-list/transaction-list/transaction-list.component';
import { CreateTransactionComponent } from './components/account-list/create-transaction/create-transaction.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidenavComponent,
    AllBudgetComponent,
    AccountListComponent,
    CreateAccountComponent,
    PayeesListComponent,
    TransactionListComponent,
    CreateTransactionComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [HttpClientModule],
  bootstrap: [AppComponent]
})
export class AppModule { }
