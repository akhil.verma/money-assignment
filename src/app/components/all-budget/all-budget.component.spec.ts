import { HttpClientModule } from '@angular/common/http';
import { DebugElement } from '@angular/core';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { CommonService } from 'src/app/shared/services/commonservices.service';
import { getTestBudgets } from 'src/testing/budget-test';

import { AllBudgetComponent } from './all-budget.component';

let component: AllBudgetComponent;
let fixture: ComponentFixture<AllBudgetComponent>;
let page: Page;

describe('AllBudgetComponent', () => {
  
  beforeEach(async(() => {
   
    TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterTestingModule],
      providers: [CommonService],
      declarations: [ AllBudgetComponent ]
    })
    .compileComponents()
    .then(createComponent);
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should display budgets',()=>{
    
    expect(page.budgetRows.length).toBeGreaterThan(0);
  });
});

/** Create the component and set the `page` test variables */
function createComponent() {
  fixture = TestBed.createComponent(AllBudgetComponent);
  component = fixture.componentInstance;

  // change detection triggers ngOnInit which gets a hero
  fixture.detectChanges();

  return fixture.whenStable().then(() => {
    
    fixture.detectChanges();
    page = new Page();
  });
}

class Page {
  /** Hero line elements */
  budgetRows: HTMLLIElement[];

  /** Highlighted DebugElement */
  highlightDe: DebugElement;

  /** Spy on router navigate method */
  navSpy: jasmine.Spy;

  constructor() {
    const budgetRowNodes = fixture.nativeElement.querySelectorAll('tr');
    this.budgetRows = Array.from(budgetRowNodes);

    // Get the component's injected router navigation spy
    const routerSpy = fixture.debugElement.injector.get(Router);
    this.navSpy = routerSpy.navigate as jasmine.Spy;
  }
}