import { Component, OnInit } from '@angular/core';
import { BudgetResponse, Budgets, ListBudget } from 'src/app/model/BudgetResponse';
import { ApiRequestGenerator } from 'src/app/Utils/ApiRequestGenerator';
import { TaskCode } from 'src/app/Utils/GlobalConstants';
import { BaseComponent } from './../../BaseComponent';
import { Router } from '@angular/router';
import { CommonService } from 'src/app/shared/services/commonservices.service';

@Component({
  selector: 'app-all-budget',
  templateUrl: './all-budget.component.html',
  styleUrls: ['./all-budget.component.css']
})
export class AllBudgetComponent extends BaseComponent implements OnInit {
  budgetList: Budgets[];

  constructor(private router: Router, private service: CommonService) {
    super(service);
   }

  ngOnInit(): void {
    this.downloadData(ApiRequestGenerator.getAllBudgets());
  }

  onResponseReceived(taskCode: TaskCode, response: any){
    const isSuccess = super.onResponseReceived(taskCode,response);
    if(isSuccess){
      const resp = response as BudgetResponse;
      this.budgetList = resp.data.budgets;
    }
    return true;
  }

  viewAssociatedAccount(id:string){
    sessionStorage.setItem('budget_id', id);
    this.router.navigateByUrl('budget-accounts');
  }
  showPayees(id:string){
    sessionStorage.setItem('budget_id',id);
    this.router.navigateByUrl('payees-data');
  }

}
