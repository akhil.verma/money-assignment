import { Component, OnInit } from '@angular/core';
import { Payee, PayeeResponse } from 'src/app/model/BudgetResponse';
import { ApiRequestGenerator } from 'src/app/Utils/ApiRequestGenerator';
import { TaskCode } from 'src/app/Utils/GlobalConstants';
import { BaseComponent } from './../../BaseComponent';
import { CommonService } from './../../shared/services/commonservices.service';

@Component({
  selector: 'app-payees-list',
  templateUrl: './payees-list.component.html',
  styleUrls: ['./payees-list.component.css']
})
export class PayeesListComponent extends BaseComponent implements OnInit {
  budgetName:string;
  budgetId:string;
  payeeId:string;
  payeesList:Payee[];
  isShow:boolean=false;

  constructor(service:CommonService) {
    super(service);
   }

  ngOnInit(): void {
    this.budgetId = sessionStorage.getItem('budget_id');
    this.budgetName = sessionStorage.getItem('budget_name');
    this.downloadData(ApiRequestGenerator.getPayeesByBudget(this.budgetId))
  }

  onResponseReceived(taskCode: TaskCode, response: any){
    const isSuccess = super.onResponseReceived(taskCode,response);
    if(isSuccess){
      switch(taskCode){
        case TaskCode.GET_PAYEES_BY_BUDGET:
          const resp = response as PayeeResponse;
          this.payeesList = resp.data.payees.filter(p => !p.deleted);
          break;
        }
      
    }
    return true;
  }
  showTransactions(payeeId:string){
    this.payeeId = payeeId;
    this.isShow=true;
  }
}
