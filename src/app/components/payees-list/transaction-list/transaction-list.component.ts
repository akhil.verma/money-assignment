import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Transaction, TransactionResponse } from 'src/app/model/BudgetResponse';
import { ApiRequestGenerator } from 'src/app/Utils/ApiRequestGenerator';
import { TaskCode } from 'src/app/Utils/GlobalConstants';
import { BaseComponent } from './../../../BaseComponent';
import { CommonService } from './../../../shared/services/commonservices.service';

@Component({
  selector: 'app-transaction-list',
  templateUrl: './transaction-list.component.html',
  styleUrls: ['./transaction-list.component.css']
})
export class TransactionListComponent extends BaseComponent implements OnInit {
  @Input() payeeId:string;
  @Output() output: EventEmitter<any> = new EventEmitter<any>();
  emitter: string;
  budgetId:string;
  transactionList:Transaction[];
  constructor(private service:CommonService) {
    super(service);
   }

  ngOnInit(): void {
    this.budgetId = sessionStorage.getItem('budget_id');
    this.downloadData(ApiRequestGenerator.getAllTrasactionByPayee(this.budgetId,this.payeeId));
  }

  onResponseReceived(taskCode: TaskCode, response: any){
    const isSuccess = super.onResponseReceived(taskCode,response);
    if(isSuccess){
      switch(taskCode){
        case TaskCode.GET_ALL_TRANSACTION:
          const resp = response as TransactionResponse;
          this.transactionList = resp.data.transactions;
          this.transactionList.filter(tData=> !tData.deleted);
          break;
        }
      
    }
    return true;
  }
  closePopup(){
    this.output.emit();
  }

}
