import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CommonService } from 'src/app/shared/services/commonservices.service';

import { PayeesListComponent } from './payees-list.component';

describe('PayeesListComponent', () => {
  let component: PayeesListComponent;
  let fixture: ComponentFixture<PayeesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterTestingModule],
      providers: [CommonService],
      declarations: [ PayeesListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PayeesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  it('fucntiopn',()=>{
    expect(component.onResponseReceived(1,1)).toBe(true);
  })

  
  it('SHOW function',()=>{
    component.showTransactions("1");
    expect(component.isShow).toBeTruthy();
  })
});
