import { Component, OnInit } from '@angular/core';
import { BaseComponent } from 'src/app/BaseComponent';
import { AccountData, AccountResponse } from 'src/app/model/BudgetResponse';
import { ApiRequestGenerator } from 'src/app/Utils/ApiRequestGenerator';
import { TaskCode } from 'src/app/Utils/GlobalConstants';
import { CommonService } from './../../shared/services/commonservices.service';

@Component({
  selector: 'app-account-list',
  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.css']
})
export class AccountListComponent extends BaseComponent implements OnInit {
  accountList:AccountData[];
  budgetId:string;
  createAccountClicked:boolean=false;
  isPaymentPopup:boolean=false;
  selectedAccountData:AccountData;
  constructor(private service:CommonService) {
    super(service);
   }

  ngOnInit(): void {
    this.budgetId = sessionStorage.getItem('budget_id')
    this.downloadData(ApiRequestGenerator.getAccountByBudgetId(this.budgetId));
  }
  
  onResponseReceived(taskCode: TaskCode, response: any){
    const isSuccess = super.onResponseReceived(taskCode,response);
    if(isSuccess){
      switch(taskCode){
        case TaskCode.GET_ACCOUNTS_BY_BUDGET:
          const resp = response as AccountResponse;
          this.accountList = resp.data.accounts;
          this.accountList.sort((a, b) => b.balance - a.balance);
          this.accountList.filter(acc=> !acc.deleted);
          break;
        }
      
    }
    return true;
  }

  createAccount(){
    this.createAccountClicked=true;
  }

  makeTransaction(account:AccountData){
    this.selectedAccountData = account;
    this.isPaymentPopup = true; 
  }

  closePopup(event:string){
    if(event == "SUCCESS"){
      this.downloadData(ApiRequestGenerator.getAccountByBudgetId(this.budgetId));
    }
    this.createAccountClicked=false;
  }
}
