import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CommonService } from 'src/app/shared/services/commonservices.service';

import { AccountListComponent } from './account-list.component';

import { AccountData } from 'src/app/model/BudgetResponse';

describe('AccountListComponent', () => {
  let component: AccountListComponent;
  let fixture: ComponentFixture<AccountListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterTestingModule],
providers: [CommonService],
      declarations: [ AccountListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('fucntion',()=>{
    component.createAccountClicked =true;
    
    component.closePopup("CLOSE");
    expect(component.createAccountClicked).toBeFalsy();
  });
  it('make transaction function',()=>{
    component.makeTransaction(new AccountData());
    expect(component.isPaymentPopup).toBeTruthy();
  })
  
});
