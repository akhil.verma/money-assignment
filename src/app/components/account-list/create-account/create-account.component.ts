import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { CreateAccountDao } from 'src/app/model/BudgetResponse';
import * as GlobalConstants from '../../../Utils/GlobalConstants'
import { BaseComponent } from '../../../BaseComponent';
import { CommonService } from '../../../shared/services/commonservices.service';
import { ApiRequestGenerator } from 'src/app/Utils/ApiRequestGenerator';
import { TaskCode } from '../../../Utils/GlobalConstants';
import { BaseResponse } from '../../../model/BaseResponse';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.css']
})
export class CreateAccountComponent extends BaseComponent implements OnInit {
  @Input() budgetId:string;
  @Output() output: EventEmitter<any> = new EventEmitter<any>();
  emitter: string;
  createAccountRequest: CreateAccountDao;
  typeList = GlobalConstants.ACCOUNT_TYPE_ARRAY;
  @ViewChild('form') public ngForm: NgForm;

  constructor(private service:CommonService) {
    super(service);
   }

  ngOnInit(): void {
    this.createAccountRequest = new CreateAccountDao();

  }

  createAccount(){
    console.log(this.createAccountRequest);
    this.downloadData(ApiRequestGenerator.createAccount(this.createAccountRequest,this.budgetId));
  }
  onResponseReceived(taskCode: TaskCode, response: any){
    const isSuccess = super.onResponseReceived(taskCode,response);
    if(isSuccess){
      switch(taskCode){
        case TaskCode.CREATE_ACCOUNT:
          const resp = response as BaseResponse;
          if(!resp.error){
            this.emitter = 'SUCCESS';
            this.output.emit(this.emitter);
          }else{
            alert("Something Went Wrong!!")
          }
         
          break;
        }
      
    }
    return true;
  }

  closePopup(){
    this.emitter = "CLOSE"
    this.output.emit(this.emitter);
  }

}
