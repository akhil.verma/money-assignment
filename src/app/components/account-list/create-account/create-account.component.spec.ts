import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { CommonService } from 'src/app/shared/services/commonservices.service';

import { CreateAccountComponent } from './create-account.component';

describe('CreateAccountComponent', () => {
  let component: CreateAccountComponent;
  let fixture: ComponentFixture<CreateAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterTestingModule,FormsModule],
      providers: [CommonService],
      declarations: [ CreateAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it( 'should check form field - invalid',async(()=>{
    fixture.whenStable().then(()=>{
      let name = component.ngForm.form.controls['name'];
      let type = component.ngForm.form.controls['type'];
      let balance = component.ngForm.form.controls['balance'];
      expect(name.valid).toBeFalsy();
      expect(type.valid).toBeFalsy();
      expect(balance.valid).toBeFalsy();
      expect(component.ngForm.valid).toBeFalsy();
    });

    
    
  }));
  it( 'should check form field - valid',async(()=>{
    fixture.whenStable().then(()=>{
      let name = component.ngForm.form.controls['name'];
      let type = component.ngForm.form.controls['type'];
      let balance = component.ngForm.form.controls['balance'];
      name.setValue('test');
      type.setValue('typetest');
      balance.setValue(2);
      expect(name.valid).toBeTruthy();
      expect(type.valid).toBeTruthy();
      expect(balance.valid).toBeTruthy();
      expect(component.ngForm.valid).toBeTruthy();
    });
  
}));
it('fucntiopn',()=>{
  expect(component.onResponseReceived(1,1)).toBe(true);
})

});
