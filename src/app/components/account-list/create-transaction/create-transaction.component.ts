import { Component, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { AccountData, Payee, PayeeResponse } from 'src/app/model/BudgetResponse';
import { Transaction } from './../../../model/BudgetResponse';
import { BaseComponent } from './../../../BaseComponent';
import { CommonService } from './../../../shared/services/commonservices.service';
import { ApiRequestGenerator } from 'src/app/Utils/ApiRequestGenerator';
import { TaskCode } from 'src/app/Utils/GlobalConstants';
import { BaseResponse } from './../../../model/BaseResponse';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-create-transaction',
  templateUrl: './create-transaction.component.html',
  styleUrls: ['./create-transaction.component.css']
})
export class CreateTransactionComponent extends BaseComponent implements OnInit {
  @Input() accountData: AccountData;
  @Output() output: EventEmitter<any> = new EventEmitter<any>();
  emitter: string;
  payeesList:Payee[];
  selectedPayee:Payee;
  transactionRequest: Transaction;
  today:string = new Date().toISOString().split("T")[0];
  budgetId:string;
  @ViewChild('form') public ngForm: NgForm;

  
  constructor(private service: CommonService) { 
    super(service)
  }

  ngOnInit(): void {
    this.transactionRequest = new Transaction();
    this.budgetId = sessionStorage.getItem("budget_id");
    this.downloadData(ApiRequestGenerator.getPayeesByBudget(this.budgetId))
    
  }

  onResponseReceived(taskCode: TaskCode, response: any){
    const isSuccess = super.onResponseReceived(taskCode,response);
    if(isSuccess){
      switch(taskCode){
        case TaskCode.GET_PAYEES_BY_BUDGET:
          const resp = response as PayeeResponse;
          this.payeesList = resp.data.payees.filter(p => !p.deleted);
          break;
        case TaskCode.CREATE_TRANSACTION:
          const resp1 = response as BaseResponse;
          if(!resp1.error){
            this.emitter = "SUCCESS";
            this.output.emit(this.emitter);
          }
          break;
        }
      
    }
    return true;
  }

  closePopup(){
    this.emitter = "CLOSE"
    this.output.emit(this.emitter);
  }

  doTransaction(){
    this.transactionRequest.account_id = this.accountData.id;
    this.transactionRequest.account_name = this.accountData.name;
    this.transactionRequest.payee_id = this.selectedPayee.id;
    this.transactionRequest.payee_name = this.selectedPayee.name;
    this.transactionRequest.amount=+this.transactionRequest.amount;
    this.transactionRequest.cleared="cleared";
    this.transactionRequest.approved = true;
    this.transactionRequest.flag_color = "red";
    console.log(this.transactionRequest);
    this.downloadData(ApiRequestGenerator.createTransaction(this.transactionRequest, this.budgetId));
  }

}
