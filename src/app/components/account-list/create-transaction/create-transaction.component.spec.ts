import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { CommonService } from 'src/app/shared/services/commonservices.service';

import { CreateTransactionComponent } from './create-transaction.component';

describe('CreateTransactionComponent', () => {
  let component: CreateTransactionComponent;
  let fixture: ComponentFixture<CreateTransactionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterTestingModule,FormsModule],
      providers: [CommonService],
      declarations: [ CreateTransactionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTransactionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form invalid when empty', () => {
    expect(component.ngForm.form.invalid).toBeFalsy();
  });

  it( 'should check form field - invalid',async(()=>{
    fixture.whenStable().then(()=>{
      let date = component.ngForm.form.controls['date'];
      let amount = component.ngForm.form.controls['amount'];
      let memo = component.ngForm.form.controls['memo'];
      expect(date.valid).toBeFalsy();
      expect(amount.valid).toBeFalsy();
      expect(memo.valid).toBeFalsy();
      expect(component.ngForm.valid).toBeFalsy();
    });  
  }));

  it( 'should check form field - valid',async(()=>{
    fixture.whenStable().then(()=>{
      let date = component.ngForm.form.controls['date'];
      let amount = component.ngForm.form.controls['amount'];
      let memo = component.ngForm.form.controls['memo'];
      date.setValue('2021-05-25');
      amount.setValue(1);
      memo.setValue("test");
      expect(date.valid).toBeTruthy();
      expect(amount.valid).toBeTruthy();
      expect(memo.valid).toBeTruthy();
      expect(component.ngForm.valid).toBeTruthy();
    });
  
  }));

  it('fucntiopn',()=>{
    expect(component.onResponseReceived(1,1)).toBe(true);
  })
});
